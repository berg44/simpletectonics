﻿Shader "Particles/Anim Alpha Blended" {

Category {
    Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" "PreviewType"="Plane" }
    SubShader {
        Pass {
        
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata_t {
                float4 vertex : POSITION;
                fixed4 color : COLOR;
            };

            struct v2f {
                float4 vertex : SV_POSITION;
                fixed4 color : COLOR;
            };

            v2f vert (appdata_t v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                //o.color = v.color;// * _TintColor;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                i.color.a =1;//*= fade;
                fixed4 col = 2.0f * i.color; //* lerp(colA, colB, i.blend);
                return col;
            }
			
            ENDCG 
        }
    }   
}
}