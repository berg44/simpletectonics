﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using System.Linq;
using simpleTectonics;

	
[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class SpiralSphere : MonoBehaviour
{
    public int n = 5000;
    public float radius = 1;
    public bool fixUvSeams = true;
    public bool createPrefab = true;
    static float goldenAngle = Mathf.PI * (3 - Mathf.Sqrt(5));
    private Mesh mesh;
	List<tectonicPlate> myPlates;
	List<tectonicPixel> myPixels;
    private List<Vector3> vertices;
    private List<Vector3> normals;
    private List<Vector2> uv;
    private List<int> triangles;
	public GameObject surfaceObj;
	public Dropdown activeSkinDropdown;
	public Text playButtonText;
	List<Color> plateColors;
	List<Color> terrainColors;
    private List<float> ys; // keeping track of the y coördinates of vertices
	private Dictionary<long, bool> edgeDict = new Dictionary<long, bool>();
	private Dictionary<long, bool> edgeDict2 = new Dictionary<long, bool>();
	private Dictionary<int, HashSet<int>> neighborDict = new Dictionary<int, HashSet<int>>();
	public GameObject arrowBody; public GameObject arrowHead;
    // This dictionary keeps track of the triangle edges. Edges have to be unique (except for the reverse edge).
    float distanceFactor = 1.5f; // This factor determines maximum distance of vertex neighbors.
                                 // the value of 1.5 is based on trial and error. With this value, triangulation works for all n > 3.
    private void Awake()
    {
		moving = false;
		instantiateColors();


		GenerateWithTexture();

		InvokeRepeating ("tectonicShift", 5f, .5f);
    }
	private void instantiateColors(){
		terrainColors = new List<Color> ();
		terrainColors.Add (new Color (51/255, 102/255, 255/255, 1));
		terrainColors.Add (new Color (0/255, 204/255, 255/255, 1));
		terrainColors.Add (new Color (0/255, 204/255, 102/255, 1));
		terrainColors.Add (new Color (153/255, 51/255, 0/255, 1));
		terrainColors.Add (new Color (191/255, 191/255, 191/255, 1));

		plateColors = new List<Color> ();
		float zero = 0;
		float one = 1;

		zero = 0f;
		one = 0.2f;
		plateColors.Add (new Color (one, zero, zero, 1));
		plateColors.Add (new Color (one, one, zero, 1));
		plateColors.Add (new Color (one, one, one, 1));
		plateColors.Add (new Color (zero, one, zero, 1));
		plateColors.Add (new Color (zero, one, one, 1));
		plateColors.Add (new Color (zero, zero, one, 1));

		zero = 0.1f;
		one = 0.3f;
		plateColors.Add (new Color (one, zero, zero, 1));
		plateColors.Add (new Color (one, one, zero, 1));
		plateColors.Add (new Color (one, one, one, 1));
		plateColors.Add (new Color (zero, one, zero, 1));
		plateColors.Add (new Color (zero, one, one, 1));
		plateColors.Add (new Color (zero, zero, one, 1));
		zero = .2f;
		one = .4f;
		plateColors.Add (new Color (one, zero, zero, 1));
		plateColors.Add (new Color (one, one, zero, 1));
		plateColors.Add (new Color (one, one, one, 1));
		plateColors.Add (new Color (zero, one, zero, 1));
		plateColors.Add (new Color (zero, one, one, 1));
		plateColors.Add (new Color (zero, zero, one, 1));
		zero = .3f;
		one = .5f;
		plateColors.Add (new Color (one, zero, zero, 1));
		plateColors.Add (new Color (one, one, zero, 1));
		plateColors.Add (new Color (one, one, one, 1));
		plateColors.Add (new Color (zero, one, zero, 1));
		plateColors.Add (new Color (zero, one, one, 1));
		plateColors.Add (new Color (zero, zero, one, 1));	
		zero = .4f;
		one = .6f;
		plateColors.Add (new Color (one, zero, zero, 1));
		plateColors.Add (new Color (one, one, zero, 1));
		plateColors.Add (new Color (one, one, one, 1));
		plateColors.Add (new Color (zero, one, zero, 1));
		plateColors.Add (new Color (zero, one, one, 1));
		plateColors.Add (new Color (zero, zero, one, 1));
	}

    

    Vector3 CircleCenter(Vector3 a, Vector3 b, Vector3 c) // This calculates the center of a circle trough all 3 vertices of a triangle.
    {                                                     // It assumes no degenerate triangles
        Vector3 t = b - a;
        Vector3 u = c - a;
        Vector3 v = c - b;

        Vector3 w = Vector3.Cross(t, u);
        float ww = w.sqrMagnitude;
        float tt = t.sqrMagnitude;
        float uu = u.sqrMagnitude;
        float iww = 0.5f / ww;

        return a + iww * (u * tt * Vector3.Dot(u, v) - t * uu * Vector3.Dot(t, v));
    }

	public void CheckEdgesAddTriangle(tectonicPixel tp1, tectonicPixel tp2, tectonicPixel tp3){
		int t1, t2, t3;

		t1 = myPixels.IndexOf (tp1);
		t2 = myPixels.IndexOf (tp2);
		t3 = myPixels.IndexOf (tp3);

		Vector3 center = CircleCenter(tp1.position.normalized, tp2.position.normalized , tp3.position.normalized ); // We calculate the center of the circle through the 3 vertices.
		Vector3 normal = Vector3.Cross(tp2.position.normalized - tp1.position.normalized, tp3.position.normalized - tp1.position.normalized);

		if (Vector3.Dot (normal, center) > 0)  // Ensure the correct winding order of the triangle.
			CheckEdgesAddTriangle (t1, t2, t3); // Check if the edges are unique and if so, add the triangle.
		else
			CheckEdgesAddTriangle (t1, t3, t2);
	}

    private void CheckEdgesAddTriangle(int t1, int t2, int t3)
    {
        long key1 = t1 + ((long)t2 *100000000);
		long key2 = t2 + ((long)t3 *100000000);
		long key3 = t3 + ((long)t1 *100000000);
		// Creating unique keys by combining two ints into a long.
		if (!edgeDict.ContainsKey (key1) && !edgeDict.ContainsKey (key2) && !edgeDict.ContainsKey (key3)) { // Ensure uniqueness of edges.
			triangles.Add (t1);
			triangles.Add (t2);
			triangles.Add (t3);
			edgeDict.Add (key1, true);
			edgeDict.Add (key2, true);
			edgeDict.Add (key3, true);

			myPixels[t1].addNeighbor(myPixels[t2]);
			myPixels[t1].addNeighbor(myPixels[t3]);
			myPixels[t2].addNeighbor(myPixels[t1]);
			myPixels[t2].addNeighbor(myPixels[t3]);
			myPixels[t3].addNeighbor(myPixels[t2]);
			myPixels[t3].addNeighbor(myPixels[t1]);
		}
    }
 
    private void RemoveZipper()
    {// The uv mapping goes wrong when phi goes from 2 pi to 0, resulting in a zipper artifact. This can be solved by duplicating a vertex.
     // By duplicating the vertex we can give it a different u-coördinate that combines correclty with the other two vertices.

        for (int i = 0; i < triangles.Count; i = i + 3)
        {
            int t1 = triangles[i];
            int t2 = triangles[i + 1];
            int t3 = triangles[i + 2];

            tectonicPixel v1 = myPixels[t1];
			tectonicPixel v2 = myPixels[t2];
			tectonicPixel v3 = myPixels[t3];
			Vector2 uvt1 = v1.getUV ();
			Vector2 uvt2 = v2.getUV ();
			Vector2 uvt3 = v3.getUV ();

			tectonicPixel repeat;
			if (v1.position.normalized.x < 0 || v2.position.normalized.x < 0 || v3.position.normalized.x < 0) // the zipper is on the positive x-side, so there is no problem here
                continue;

			if (v1.position.normalized.z >= 0 && v2.position.normalized.z >= 0 && v3.position.normalized.z >= 0) // if all  vertices are on the positive z-side there is no problem
                continue;

			else if (v1.position.normalized.z < 0 && v2.position.normalized.z < 0 && v3.position.normalized.z < 0) // if all  vertices are on the negative  z-side there is no problem
                continue;

			else if (v1.position.normalized.z >= 0 && v2.position.normalized.z < 0 && v3.position.normalized.z < 0) // v1 has to be duplicated
            {
				myPixels.Add (new tectonicPixel (v1));
				myPixels [myPixels.Count - 1].setUV (new Vector2 (1 + uvt1.x, uvt1.y));
				v1.linkedPixel = myPixels [myPixels.Count - 1];
                triangles[i] = myPixels.Count-1;
            }
            else if (v1.position.normalized.z < 0 && v2.position.normalized.z >= 0 && v3.position.normalized.z < 0) // v2 has to be duplicated
            {
				myPixels.Add (new tectonicPixel (v2));
				myPixels [myPixels.Count - 1].setUV (new Vector2 (1 + uvt2.x, uvt2.y));
				v2.linkedPixel  = myPixels [myPixels.Count - 1];
				triangles[i + 1] = myPixels.Count-1;
            }
            else if (v1.position.normalized.z < 0 && v2.position.normalized.z < 0 && v3.position.normalized.z >= 0) // v3 has to be duplicated
            {
				myPixels.Add (new tectonicPixel (v3));
				myPixels [myPixels.Count - 1].setUV (new Vector2 (1 + uvt3.x, uvt3.y));
				v3.linkedPixel = myPixels [myPixels.Count - 1];
				triangles[i + 2] = myPixels.Count-1;
            }
            else if (v1.position.normalized.z < 0 && v2.position.normalized.z >= 0 && v3.position.normalized.z >= 0) // v1 has to be duplicated 
            {
				myPixels.Add (new tectonicPixel (v1));
				myPixels [myPixels.Count - 1].setUV (new Vector2 (1 + uvt1.x, uvt1.y));
				v1.linkedPixel = myPixels [myPixels.Count - 1];
				triangles[i] = myPixels.Count-1;
            }
            else if (v1.position.normalized.z >= 0 && v2.position.normalized.z < 0 && v3.position.normalized.z >= 0) // v2 has to be duplicated
            {
				myPixels.Add (new tectonicPixel (v2));
				myPixels [myPixels.Count - 1].setUV (new Vector2 (1 + uvt2.x, uvt2.y));
				v2.linkedPixel = myPixels [myPixels.Count - 1];
				triangles[i + 1] = myPixels.Count-1;
            }
            else                                        // v3 has to be duplicated
            {
				myPixels.Add (new tectonicPixel (v3));
				myPixels [myPixels.Count - 1].setUV (new Vector2 (1 + uvt3.x, uvt3.y));
				v3.linkedPixel = myPixels [myPixels.Count - 1];
				triangles[i + 2] = myPixels.Count-1;
            }
        }
    }

    private void ReplacePoles()
    {
        // the poles both have triangle that goes around the pole, creating a uv artifact
        // we remove those triangles and replace them with 3 smaller ones, connected to the pole

        int n1 = triangles[triangles.Count - 1]; // getting the last triangle ints
        int n2 = triangles[triangles.Count - 2];
        int n3 = triangles[triangles.Count - 3];

        triangles.RemoveRange(0, 3); // removing the first triangle
        triangles.RemoveRange(triangles.Count - 3, 3); // removing the last triangle

        BottomPoleTriangle(0, 1); // replacing the first triangle connected to the pole
        BottomPoleTriangle(1, 2); // replacing the second triangle connected to the pole

        // the last triangle is special, because one of the vertices has u-coördinate 0, but has to have 1 when connected from the other side.
        triangles.Add(2); 
        triangles.Add(myPixels.Count);
		myPixels.Add (new tectonicPixel (myPixels[0]));
		myPixels [myPixels.Count - 1].setUV (new Vector2(1, myPixels[0].getUV().y));
		myPixels [0].linkedPixel = myPixels [myPixels.Count - 1];

        triangles.Add(myPixels.Count);
		myPixels.Add (new tectonicPixel (new Vector3(0, -1, 0) * radius));
		myPixels [myPixels.Count - 1].setUV (new Vector2((1 + myPixels[2].getUV().x) / 2, 0));

		float u1 = myPixels [n1].getUV().x; // getting the u coördinates around the top pole.
		float u2 = myPixels [n2].getUV().x;
		float u3 = myPixels [n3].getUV().x;

        int low, med, high;

        // The difficulty is in determining the winding order of the original 3 triangles.
        // We solve this by ordering them by u coordinate.

        if (u1 > u2)
        {
            if (u2 > u3)
            {
                low = n3; med = n2; high = n1;
            }
            else if (u1 > u3)
            {
                low = n2; med = n3; high = n1;
            }
            else
            {
                low = n2; med = n1; high = n3;
            }
        }
        else
        {
            if (u1 > u3)
            {
                low = n3; med = n1; high = n2;
            }
            else if (u2 > u3)
            {
                low = n1; med = n3; high = n2;
            }
            else
            {
                low = n1; med = n2; high = n3;
            }
        }

        TopPoleTriangle(low, med);
        TopPoleTriangle(med, high);

        // the last triangle is special, because the last vertex needs to be duplicated, adding 1 to the u-coördinate.

        triangles.Add(high);

        triangles.Add(myPixels.Count);
		myPixels.Add (new tectonicPixel (new Vector3(0, -1, 0) * radius));
		myPixels [myPixels.Count - 1].setUV (new Vector2((myPixels [high].getUV().x + myPixels[low].getUV().x + 1) / 2, 1));

		triangles.Add(myPixels.Count);
		myPixels.Add (new tectonicPixel (myPixels[low]));
		myPixels [myPixels.Count - 1].setUV (new Vector2(myPixels [low].getUV().x + 1, myPixels [low].getUV().y));
		myPixels [low].linkedPixel = myPixels [myPixels.Count - 1];
    }

    private void BottomPoleTriangle(int t1, int t2)
    {
        triangles.Add(t1); // creating a triangle from 2 vertices and the bottom pole
        triangles.Add(t2);
        triangles.Add(myPixels.Count);
		myPixels.Add (new tectonicPixel (new Vector3(0, -1, 0) * radius));
		myPixels [myPixels.Count - 1].setUV (new Vector2((myPixels[t1].getUV().x + myPixels [t2].getUV().x) / 2f, 0));
    }

    private void TopPoleTriangle(int t1, int t2)
    {
        triangles.Add(t1); // creating a triangle from 2 vertices and the top pole
        triangles.Add(myPixels.Count);
        triangles.Add(t2);
		myPixels.Add (new tectonicPixel (new Vector3(0, -1, 0) * radius));
		myPixels [myPixels.Count - 1].setUV (new Vector2((myPixels[t1].getUV().x + myPixels [t2].getUV().x) / 2f, 1));
    }

	private void writeTriangles(){
		
		triangles.Clear ();
		edgeDict.Clear ();
		neighborDict.Clear ();
		float maxDist = Mathf.Sqrt(4 * Mathf.PI / n) * distanceFactor * radius; // We add as few neighbors as possible for performance reasons
		float maxDistSq = maxDist * maxDist; 

		vertices = myPixels.Select(x=>x.position).ToList();
		ys = myPixels.Select (x => x.position.y).ToList ();
		int counter = 0;
		for (int i = 0; i < myPixels.Count; i++)
		{
			myPixels [i].clearNeighbors ();
			maxDist = Mathf.Sqrt(4 * Mathf.PI / n) * distanceFactor * radius;
			maxDistSq = maxDist * maxDist; 
			Vector3 vi = vertices[i];
			List<int> neighbors = new List<int>();
			int breakout = 0;
			while (neighbors.Count < 2 && breakout < 10) {
				neighbors = new List<int>();
				for (int j = i + 1; j < n; j++) {
					float dy = ys [j] - ys [i]; // We calculate how far above the current vertex the other vertex is located.
					if (dy * radius > maxDist) // If other vertices are too far above the current vertex, we stop looping.
					continue;
					else if ((vi - vertices [j]).sqrMagnitude < maxDistSq) // if Ihe neighbor is within range, we add it to the list.
					neighbors.Add (j);
				}
				breakout++;
				maxDist*=1.1f;
				maxDistSq = maxDist * maxDist;
			}

			int trianglesAdded=0;
			float smallResult = 0f;
			for (int j = 0; j < neighbors.Count; j++) // Now we traverse the list to find potential triangle candidates.
			{
				trianglesAdded =0;
				int nj = neighbors[j]; // We pick a first neighbor on the list
				Vector3 vj = vertices[nj];
				for (int k = j + 1; k < neighbors.Count; k++) // We pick a second neighbor on the list.
				{
					int nk = neighbors[k];
					Vector3 vk = vertices[nk];

					Vector3 center = CircleCenter(vi, vj, vk); // We calculate the center of the circle through the 3 vertices.
					float radiusSq = (center - vi).sqrMagnitude; // We calculate the radius of the circle.

					bool validTriangle = true; 
					for (int l = 0; l < neighbors.Count; l++) // We check if any other vertex is within a circle-radius of the center.
					{
						if (l != j && l != k) // Only other vertices are considered (usually no more than 1 or 2)
						{
							smallResult = (vertices [neighbors [l]] - center).sqrMagnitude;
							if ((vertices[neighbors[l]] - center).sqrMagnitude < radiusSq)  
							{
								validTriangle = false; // The triangle is rejected, another vertex is too close.
								break;                // This rejection principle comes from Delaunay triangulation
							}
						}
					}

					if (validTriangle)
					{
						Vector3 normal = Vector3.Cross(vj - vi, vk - vi);
						if (Vector3.Dot (normal, center) > 0) { // Ensure the correct winding order of the triangle.
							CheckEdgesAddTriangle (i, nj, nk); // Check if the edges are unique and if so, add the triangle.
							trianglesAdded++;
						} else {
							CheckEdgesAddTriangle (i, nk, nj);
							trianglesAdded++;
						}
					}
				}
			}
		}
	}

	public List<int> addNeighbors(tectonicPixel centerP, List<int> neighbors){
		Vector3 center = centerP.position.normalized;
		Vector3 result = new Vector3 (0, 0, 0);
		if (neighbors.Count == 5){
			foreach (int nn in neighbors)
				result+= myPixels [nn].position.normalized-center;
			//result now points in the opposite direction of the hole so subtract it from the original
			result = center - result;
			myPixels.Add(new tectonicPixel(result));
			neighbors.Add (myPixels.Count-1);
			vertices.Add (result);
		}
		return neighbors;
	}


	private void GenerateWithTexture()
	{
		if (n < 4)
			n = 4;
		if (n > 60000)
			n = 60000;

		//REPLACE ALL INDIVIDUALS WITH PIXELS
		myPixels = new List<tectonicPixel>();
		vertices = new List<Vector3>();
		normals = new List<Vector3>();
		uv = new List<Vector2>();
		ys = new List<float>();
		triangles = new List<int>();
		GetComponent<MeshFilter>().mesh = mesh = new Mesh();
		mesh.name = "Spiral Sphere";

		float off = 2 / (float)n;
		for (int i = 0; i < n; i++)
		{
			// This is the golden spiral algorithm. The points on the sphere are calculated in order of increasing y
			float y = i * off - 1 + (off / 2);
			//ys.Add(y);
			float r = Mathf.Sqrt(1 - y * y);
			float phi = (i * goldenAngle) % (2 * Mathf.PI);
			Vector3 vertex = new Vector3(Mathf.Cos(phi) * r, y, Mathf.Sin(phi) * r);
			tectonicPixel newPixel = new tectonicPixel (vertex);
			newPixel.setUV (new Vector2 (0.5f * phi / Mathf.PI, (Mathf.PI - Mathf.Acos (y)) / Mathf.PI));
			myPixels.Add (newPixel);
			//vertices.Add(vertex * radius);
			//normals.Add(vertex.normalized);
			//uv.Add(new Vector2(0.5f * phi / Mathf.PI, (Mathf.PI - Mathf.Acos(y)) / Mathf.PI)); // Using polar coordinates converted to uv
		}

		newPlates();

		writeTriangles ();

		for (int i = 0; i < myPixels.Count; i++) {
			myPixels [i].setEdgeStatus ();
			float randomizer;
			randomizer = Random.Range (0.98f, 1.0f);
			randomizer *= randomizer;
			randomizer *= randomizer;
			randomizer *= randomizer;

			randomizer = Mathf.PerlinNoise (Mathf.Asin(myPixels [i].position.x)*10 + Mathf.Acos(myPixels [i].position.z)*Mathf.PI, myPixels [i].position.y*8);
			randomizer = randomizer / 4.0f + 0.8f;
			//myPixels [i].position *= randomizer*1.05f;			
		}

		foreach (tectonicPlate plate in myPlates)
			plate.connectEdges ();
		

		//if (fixUvSeams)
		//{
		RemoveZipper();
		ReplacePoles();
		//}
		updatePlates ();//catch any new pixels in the zipper or across image seams

		vertices = myPixels.Select(x=>x.position).ToList();
		normals = myPixels.Select (x => x.position.normalized).ToList ();
		uv = myPixels.Select (x => x.uv).ToList ();
		Color[] colors = updateColors ();

		mesh.vertices = vertices.ToArray();
		mesh.triangles = triangles.ToArray();
		mesh.normals = normals.ToArray();
		mesh.uv = uv.ToArray();
		mesh.colors = colors;

	}

	public void newPlates(){
		 myPlates = new List<tectonicPlate> ();
		int numberOfSeeds = 15;

		for (int i = 0; i < numberOfSeeds; i++) {
			tectonicPlate newPlate = new tectonicPlate (i,this);
			int index = Random.Range(0, myPixels.Count);
			if (!myPixels [index].isClaimed ())
				claimPixel (myPixels [index], newPlate);

			newPlate.setColor (plateColors [i]);
			myPlates.Add (newPlate);
		}
		updatePlates ();
	}

	public void updatePlates(){
		for (int i = 0; i < myPixels.Count; i++) {
			tectonicPixel thePixel = myPixels [i];
			tectonicPlate closestPlate = myPlates [0];
			float bestDistance = (closestPlate.pixels [0].position - thePixel.position).magnitude;
			for (int j = 1; j < myPlates.Count; j++) {
				float thisDistance = (myPlates [j].pixels [0].position - thePixel.position).magnitude;
				if (thisDistance < bestDistance) {
					bestDistance = thisDistance;
					closestPlate = myPlates [j];
				}	
			}
			claimPixel (thePixel, closestPlate);
		}
		Debug.Log ("Plates updated");

	}

	public void claimPixel(tectonicPixel pix, tectonicPlate plate){
		pix.setPlate (plate);
		if (!plate.pixels.Contains(pix))
			plate.addPixel (pix);
	}

	public List<tectonicPixel> getNeighborPixels(tectonicPixel root){
		List<int> neighborInts = getNeighbors (root);
		List<tectonicPixel> results = new List<tectonicPixel >();
		foreach (int i in neighborInts)
			results.Add (myPixels [i]);
		return results;
	}

	public HashSet<int> getOriginalNeighbors(int index){
		if (!neighborDict.ContainsKey (index)) {
			HashSet<int> myHash = new HashSet<int> ();
			foreach (int nn in getNeighbors(myPixels[index])) {
				if (!myHash.Contains(nn))
					myHash.Add (nn);
			}
			neighborDict.Add(index, myHash);
		}
		return neighborDict [index];
	}

	public List<int> getNeighbors(tectonicPixel root){
		return getNeighbors (root, 1.0f);
	}

	public List<int> getNeighbors(tectonicPixel root, float scalar){
		//assumes myPixels is currently sorted in order of ascending Ys
		List<int> neighbors = new List<int>();
		float maxDist = scalar * Mathf.Sqrt(4 * Mathf.PI / n) * distanceFactor * radius; // We add as few neighbors as possible for performance reasons
		float maxDistSq = maxDist * maxDist; 
		Vector3 vi = root.position.normalized;
		for (int j = 0; j < n; j++)
			if ((vi - myPixels [j].position.normalized).sqrMagnitude < maxDistSq) // if Ihe neighbor is within range, we add it to the list.
				neighbors.Add(j);

		return neighbors;
	}

	public void updatePlanetColors(){
		mesh.colors = updateColors();
	}

	public Color[] updateColors(){
		Color[] colors = new Color[myPixels.Count];
		if (activeSkinDropdown == null) {
			//TECTONIC PLATE
			Debug.Log("no skin active!!");
			for (int i = 0; i < myPixels.Count; i++) {
				colors [i] = myPixels[i].getPlate().getColor();
			}
		} else if (activeSkinDropdown.value == 0) { 
			//ALTITUDE
			for (int i = 0; i < myPixels.Count; i++) {
				colors [i] = Color.Lerp (Color.blue, Color.gray, (myPixels[i].position.magnitude - 1f * radius) / (0.10f * radius));
			}
		} else if (activeSkinDropdown.value == 1) {
			//TECTONIC PLATE
			for (int i = 0; i < myPixels.Count; i++) {
				colors [i] = myPixels[i].getPlate().getColor();
			}
		} else {
			//COVER TYPE (deep, shallow, flat, mountain, glacier)
			for (int i = 0; i < myPixels.Count; i++) {
				if (myPixels [i].position.magnitude < radius*0.97f)
					colors [i] = terrainColors [0];
				else if (myPixels [i].position.magnitude < radius*0.985f)
					colors [i] = terrainColors [1];
				else if (myPixels [i].position.magnitude < radius*1.0f)
					colors [i] = terrainColors [2];
				else //if (myPixels [i].position.magnitude < radius*0.97)
					colors [i] = terrainColors [3];
				//colors[i]= new Color(.25f,.25f,0);
				//else if (myPixels [i].position.magnitude < radius*0.97)
				//	colors [i] = terrainColors [0];
			}
		}
		return colors;
	}

	public void redoTriangles(){
		writeTriangles ();
		Color[] colors = updateColors ();

		Debug.Log ("vertices" + vertices.Count);
		mesh.vertices = vertices.ToArray();
		Debug.Log ("triangles" + triangles.Count);
		mesh.triangles = triangles.ToArray();
		Debug.Log ("normals" + normals.Count);
		mesh.normals = normals.ToArray();
		Debug.Log ("uvs" + uv.Count );
		mesh.uv = uv.ToArray();
		Debug.Log ("colors" + colors.Length );
		mesh.colors = colors;
	}

	public bool moving;
	public void MovePixels(){
		if (!moving) {
			playButtonText.text = "Pause";
		} else {
			playButtonText.text = "Move Plates";
		}
		moving = !moving;
		Debug.Log ("Moving: " + moving);
	}

	public void keepShifting(){

	}

	public void tectonicShift(){
		if (!moving)
			return;
		
		float startTime = Time.time;
		foreach (tectonicPlate movingPlate in myPlates) {
			movingPlate.randomDirection (radius);
			float dPhiDT = movingPlate.getDirection ().x;
			float dThetaDT = movingPlate.getDirection ().y;

			float phi;
			float theta;
			float mag;
			Vector3 oldPosition;
			Vector3 plateCenter = new Vector3(0,0,0);
			foreach (tectonicPixel pix in movingPlate.pixels) {
				//if (pix.duplicatePixel)
				//	continue;
				oldPosition = pix.position;
				mag = pix.position.magnitude;
				phi = Mathf.Asin (pix.position.normalized.x);
				theta = Mathf.Acos (pix.position.normalized.y);

				pix.position.z += radius * dPhiDT * Mathf.Cos (phi);
				pix.position.y += radius * dThetaDT * Mathf.Sin (theta);
				pix.position.x += radius * dPhiDT * Mathf.Sin (phi);
				pix.position = mag * pix.position.normalized;//keep the same magnitude as before

				pix.uv = new Vector2(0.5f * phi / Mathf.PI, (Mathf.PI - Mathf.Acos(pix.position.normalized.y)) / Mathf.PI); // Using polar coordinates converted to uv
				pix.lastStep = pix.position - oldPosition;
				//if (pix.linkedPixel != null) {
					//Debug.Log ("bringing duplicated pixel");
					//pix.linkedPixel.position = pix.position;
				//}
				plateCenter += pix.position;
				float closestNeighbor = pix.closestNeighbor ();
			}
			if (movingPlate.pixels.Count>0)
				plateCenter /= movingPlate.pixels.Count;
		}
		//displayDirections ();
		bool minorMoves = true;
		if (!adjustOverlap()){
			Debug.Log ("MAJOR MOVES UNDERWAY!!");
			minorMoves = false;
			myPixels.Sort(delegate(tectonicPixel c1, tectonicPixel c2) { return c1.position.y.CompareTo(c2.position.y); });//organize by top to bottom
			RemoveOverlap ();
			//HACK: create new pixels here
			n = myPixels.Count;
			vertices = myPixels.Select(x=>x.position).ToList();
			normals = myPixels.Select (x => x.position.normalized).ToList ();
			uv = myPixels.Select (x => x.uv).ToList ();
			//Don't sort after writing triangles... that messes everything up.
			writeTriangles ();
			//RemoveZipper();
			//ReplacePoles();
			updatePlates ();
			normals = myPixels.Select (x => x.position.normalized).ToList ();
			uv = myPixels.Select (x => x.uv).ToList ();
		}

		vertices = myPixels.Select(x=>x.position).ToList();

		Color[] colors = updateColors ();

		Debug.Log ("vertices" + vertices.Count);
		Debug.Log ("triangles" + triangles.Count);

		if (minorMoves) {
			mesh.vertices = vertices.ToArray ();
			mesh.colors = colors;
		} else {
			mesh.Clear ();
			mesh.vertices = vertices.ToArray ();
			mesh.triangles = triangles.ToArray ();
			mesh.normals = normals.ToArray ();
			mesh.uv = uv.ToArray ();
			mesh.colors = colors;
		}
		Debug.Log ("done " + (Time.time - startTime)*1000f);
	}

	public void displayDirections(){
		GameObject arrowbody;
		GameObject arrowhead;
		foreach (tectonicPlate plate in myPlates) {
			float dPhiDT = plate.getDirection ().x;
			float dThetaDT = plate.getDirection ().y;
			float phi;
			float theta;
			phi = Mathf.Asin (plate.center.normalized.x);
			theta = Mathf.Acos (plate.center.normalized.y);

			Vector3 direction3 = new Vector3(radius * dPhiDT * Mathf.Sin (phi),radius * dThetaDT * Mathf.Sin (theta),radius * dPhiDT * Mathf.Cos (phi));
			Debug.Log ("making arrow");
			arrowbody = GameObject.Instantiate (arrowBody);
			//arrowhead = GameObject.Instantiate (arrowHead);
			arrowbody.transform.position = plate.center;
			//arrowbody.transform.rotation = Quaternion.LookRotation(direction3,Vector3.up);
			//arrowhead.transform.position = plate.center + direction3;
			//arrowhead.transform.rotation = Quaternion.LookRotation(direction3,Vector3.up);
		}
	}

	public bool adjustOverlap(){
		int i = 0;
		HashSet <int> neighbors;
		float minDist = Mathf.Sqrt(4 * Mathf.PI / n) * distanceFactor * radius/6; 
		float minDistSq = minDist * minDist; 
		tectonicPixel activePixel; tectonicPlate activePlate;
		float movesUp = 0;
		float plateNeighborElevation = 0;
		int plateNeighbors = 0;
		while (i < myPixels.Count) {
			plateNeighbors = 0;
			plateNeighborElevation = 0;
			activePixel = myPixels [i];
			activePlate = activePixel.getPlate ();
			Vector3 toCenter = activePixel.position.normalized - activePlate.center.normalized;
			neighbors = getOriginalNeighbors(i);
			//if (neighbors.Count > 20)
			//	return false;
			//check if the last step crossed into foreign plate boundaries
			bool intersectingPlate = false;
			plateNeighborElevation = 0;
			movesUp = 0;
			foreach (int neighborIndex in neighbors) {
				tectonicPixel neighborPixel = myPixels[neighborIndex];
				Vector3 toNeighbor = activePixel.position.normalized - neighborPixel.position.normalized;
				if (neighborPixel.getPlate () != activePlate) {
					//is the neighbor inbetween the pixel and the plate center
					if (Vector3.Dot (toCenter, toNeighbor) >= 0) {
						intersectingPlate = true;
						if (neighborPixel.getPlate ().getFloatScore () > activePlate.getFloatScore ()
							   && Vector3.Dot (activePixel.lastStep, toNeighbor) >= 0)
							movesUp = 1;//LATER: figure out how to scale and change magnitude of upward thrust;
						else
							movesUp = 0; 
						break; 
					}
				} else {
					plateNeighborElevation += neighborPixel.position.magnitude;
					plateNeighbors++;
					//TODO: make sure we don't overrun pixels in the same plate
				}
					
			}
			if (plateNeighbors > 0) {
				plateNeighborElevation /= neighbors.Count;
				if (plateNeighborElevation < 1)
					plateNeighborElevation = 1;
			}

			//take half of the last step if we crossed boundaries ...
			//TODO: handle the case where the step size was already bigger than previous pixel step
			if (intersectingPlate) {
				activePixel.position -= 0.5f * activePixel.lastStep;
				//figure out if this pixel goes up or stays at the same elevation
			}

			activePixel.position *= (1f + movesUp / 100f);
			if (plateNeighborElevation > 0) {
				activePixel.position = activePixel.position.normalized * (plateNeighborElevation);
			}
			if (activePixel.position.magnitude > 1.08f)
				activePixel.position *= 1.08f / activePixel.position.magnitude;
			else if (activePixel.position.magnitude < 1.0f)
				activePixel.position *= 1.0f / activePixel.position.magnitude;
			i++;
		}

		return true;
	}

	public void RemoveOverlap(){
		//not used currently because it is pretty slow - minimize number of uploads to the mesh.
		int i = 0;
		int j = 0;
		float maxDist = Mathf.Sqrt(4 * Mathf.PI / n) * distanceFactor * radius; // We add as few neighbors as possible for performance reasons
		float maxDistSq = maxDist * maxDist; 
		//smaller min dist
		float minDist = Mathf.Sqrt(4 * Mathf.PI / n) * distanceFactor * radius/4; 
		float minDistSq = minDist * minDist; 

		Vector3 vi;
		Vector3 vj;
		foreach (tectonicPlate plate in myPlates) {
			Vector3 fakeCenter;
			if (plate.pixels.Count > 10) {
				fakeCenter = plate.pixels [0].position.normalized
				+ plate.pixels [(int)(plate.pixels.Count / 2)].position.normalized
				+ plate.pixels [plate.pixels.Count - 1].position.normalized;
				fakeCenter /= 3;
			} else {
				fakeCenter = plate.pixels [0].position.normalized ;
			}
			i = 0;
			while(i<plate.pixels.Count){
				vi = plate.pixels[i].position.normalized  ;
				j = i+1;
				while( j < plate.pixels.Count) {
					//if pixels are too scrunched up, delete one and make new triangles.
					vj = plate.pixels [j].position.normalized;
					if ((vi - vj).sqrMagnitude < minDistSq) {// if Ihe neighbor is within range, we add it to the list.
						//delete whichever is closer to the fakecenter
						if ((vi - fakeCenter).sqrMagnitude >= (vj - fakeCenter).sqrMagnitude) {
							deletePixel(plate.pixels[j]);
						} else {
							deletePixel (plate.pixels [i]);
							j = plate.pixels.Count;
							i--;
						}							
					}
					j++;
				}
				i++;
			}
		}

		ys = myPixels.Select(x=>x.position.normalized.y).ToList();
		
		List<int> neighbors = new List<int>();
		Dictionary <int, List<int>> garbageCollect = new Dictionary<int,List<int>> ();

		while (i < myPixels.Count) {
			vi = myPixels [i].position.normalized;
			tectonicPlate currentPlate = myPixels [i].getPlate ();
			neighbors.Clear ();
			for (j = i + 1; j < myPixels.Count; j++) {
				float dy = ys [j] - ys [i]; // We calculate how far above the current vertex the other vertex is located.
				if (dy * radius > maxDist) // If other vertices are too far above the current vertex, we stop looping.
					continue;				
				vj = myPixels [j].position.normalized;
				//if pixels from two plates are overlapping, one plate will dominate the other. add these to a list to process later. 
				if ((vi - vj).sqrMagnitude < maxDistSq) {
					if (myPixels [j].getPlate () != currentPlate) {
						if (garbageCollect.ContainsKey (i))
							(garbageCollect [i]).Add (j);
						else
							garbageCollect.Add (i, new List<int>{ j });

					}
				}
			}
			i++;
		}

		HashSet <tectonicPixel> deleteMe = new HashSet<tectonicPixel> ();
		foreach(int firstPixIndex in garbageCollect.Keys) {
			//TODO: come up with better FLOAT SCORE decision about which pixel gets raised and which is subducted
			int floatScore = myPlates.IndexOf (myPixels [firstPixIndex ].getPlate ());
			foreach (int secondPixIndex in garbageCollect[firstPixIndex]) {
				int otherFloatScore = myPlates.IndexOf (myPixels [secondPixIndex].getPlate ());
				if (otherFloatScore < floatScore) {
					if(!deleteMe.Contains(myPixels[secondPixIndex]))
						deleteMe.Add (myPixels[secondPixIndex]);
					//ASCENDPIXEL was a stub, not currently used - see AdjustOverlap sub
					//ascendPixel(myPixels[firstPixIndex]);
				} else {
					if(!deleteMe.Contains(myPixels[firstPixIndex] ))
						deleteMe.Add (myPixels[firstPixIndex] );
					//ascendPixel(myPixels[secondPixIndex]);
				}
			}
		}
		Debug.Log ("ys: " + ys.Count + ", pix: " + myPixels.Count);
		foreach (tectonicPixel dm in deleteMe)
			deletePixel (dm);
	}

	public void deletePixel(tectonicPixel deletePixel){
		int dIndex = myPixels.IndexOf (deletePixel);
		deletePixelAt (dIndex);
	}

	public void deletePixelAt(int deletePixelLocation){
		if (deletePixelLocation < 0 || deletePixelLocation >= myPixels.Count)
			return;
		tectonicPlate plate = myPixels [deletePixelLocation].getPlate ();
		plate.pixels.Remove (myPixels [deletePixelLocation]);
		myPixels.RemoveAt (deletePixelLocation);
		if (plate.pixels.Count < 50) {
			//TODO:combine the plate with an adjacent one if it is too small.
		}
	}		
}
