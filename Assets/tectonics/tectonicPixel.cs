﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace simpleTectonics
{
	public class tectonicPixel
	{
		float oldHeight;
		tectonicPlate plate;
		HashSet<tectonicPixel> neighbors;
		bool _isEdgePixel;
		bool _isDuplicatePixel;
		public Vector3 position;
		public Vector3 lastStep;
		public Vector2 uv;
		public tectonicPixel  linkedPixel;

		public tectonicPixel(){ //put all common code between constructors here
			linkedPixel = null;
			_isDuplicatePixel = false;
			_isEdgePixel = false;
			neighbors = new HashSet<tectonicPixel> ();
		}

		public tectonicPixel(tectonicPixel dup):this(){
			position = dup.position;
			oldHeight = dup.oldHeight;
			plate = dup.getPlate ();
			if (plate!=null)
				plate.addPixel (this);
			uv = dup.uv;
			_isDuplicatePixel = true;
		}

		public tectonicPixel (Vector3 myposition):this()
		{
			position = myposition;
			plate = null;
			uv = new Vector2(0.5f * Mathf.Asin (position.normalized.x) / Mathf.PI, (Mathf.PI - Mathf.Acos(position.normalized.y)) / Mathf.PI); // Using polar coordinates converted to uv
		}

		public void setPlate(tectonicPlate myPlate){
			plate = myPlate;
		}

		public tectonicPlate getPlate(){
			return plate;
		}

		public bool isClaimed(){
			return plate != null;
		}
		public void setUV(Vector2 newUV){
			uv = newUV;
		}
		public Vector2 getUV(){
			return uv;
		}

		public void addNeighbor(tectonicPixel neighbor){
			if (!neighbors.Contains (neighbor))
				neighbors.Add (neighbor);
		}
		public HashSet<tectonicPixel> getNeighbors(){
			return neighbors;
		}
		public void clearNeighbors(){
			neighbors.Clear ();
		}
		public float closestNeighbor(){
			return 1f;
		}
		public bool isNeighbor(tectonicPixel pixelInQuestion){
			if (neighbors.Contains(pixelInQuestion))
				return true;
			else
				return false;
		}

		public void setEdgeStatus(){				
			_isEdgePixel = false;
			if (_isDuplicatePixel)
				return;
			foreach (tectonicPixel nn in neighbors) {
				if (nn.getPlate () != plate)
					_isEdgePixel = true;
			}
		}
		public bool isEdgePixel(){
			return _isEdgePixel;
		}
		public bool isDuplicatePixel(){
			return _isDuplicatePixel;
		}

	}
}

