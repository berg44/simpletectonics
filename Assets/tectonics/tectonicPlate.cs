﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

namespace simpleTectonics
{
	public class tectonicPlate
	{
		Vector2 direction;
		float speed;
		public List <tectonicPixel> pixels;
		Color color;
		public Vector3 center;
		float floatScore;
		SpiralSphere mySphere;
		public tectonicPlate (int _floatScore, SpiralSphere _mySphere)
		{
			pixels = new List<tectonicPixel> ();
			direction = new Vector2 (0, 0);
			floatScore = _floatScore;
			mySphere = _mySphere;
		}

		public void addPixel(tectonicPixel newPix){
			pixels.Add (newPix);
			//Debug.Log ("Added pixel:" + newPix.position.x + ", " + newPix.position.y + ", " + newPix.position.z);
		}

		public float getFloatScore(){
			return floatScore;
		}

		public void setColor(Color newColor){
			color = newColor;
		}
		public Color getColor(){
			return color ;
		}
		public void randomDirection(float radius){
			if (direction.magnitude!=0)
				return;
			float dPhiDT = Random.Range (0, 2 * Mathf.PI)*0.001f;
			float dThetaDT = Random.Range (0, 2 * Mathf.PI)*0.001f;
			direction = new Vector2 (dPhiDT, dThetaDT);
		}
		public Vector2 getDirection (){
			return direction;
		}

		public void connectEdges(){
			List<tectonicPixel> edgePixels = new List<tectonicPixel> ();
			foreach (tectonicPixel eachPixel in pixels)
				if (eachPixel.isEdgePixel ())
					edgePixels.Add (eachPixel);
			//no duplicate pixel can be an edge pixel! handled in pixel class
			for(int i = 0; i< edgePixels.Count; i++) {
				tectonicPixel activePixel = edgePixels[i];
				tectonicPixel neighbor1=edgePixels[0]; tectonicPixel neighbor2=edgePixels[0];
				float closestDistance = 10;
				//get the closest 2 neighbors
				for (int j = 0; j<edgePixels.Count; j++){
					if (j == i)
						continue;
					tectonicPixel currentNeighbor = edgePixels [i];
					if ((currentNeighbor.position.normalized - activePixel.position.normalized).magnitude < closestDistance) {
						neighbor2 = neighbor1;
						neighbor1 = edgePixels [j];
					}
				}
				if (neighbor1 == neighbor2)
					continue;
				if (!activePixel.isNeighbor (neighbor1)) {
					foreach (tectonicPixel neighborsNeighbor in neighbor1.getNeighbors())
						if (activePixel.isNeighbor (neighborsNeighbor))
							mySphere.CheckEdgesAddTriangle (activePixel, neighborsNeighbor, neighbor1);
				}
				if (!activePixel.isNeighbor (neighbor2)) {
					foreach (tectonicPixel neighborsNeighbor in neighbor2.getNeighbors())
						if (activePixel.isNeighbor (neighborsNeighbor))
							mySphere.CheckEdgesAddTriangle (activePixel, neighborsNeighbor, neighbor2);
				}

			}
		}
	}
}

